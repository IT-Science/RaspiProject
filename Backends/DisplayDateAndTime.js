function display_c(){
    var refresh=60000; // Refresh rate in milli seconds
    mytime=setTimeout('display_timedate()',refresh);
}
function addZero(number){
    if (number.toString().length === 1){
        return "0" + number;
        
    } else{
        return number
    }
}
function display_timedate() {
    var strcount;
    var days = ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'];
    var x = new Date();

    var date= days[x.getDay()] + ", " + addZero(x.getDate()) + "." + addZero((parseInt(x.getMonth()) + 1)) + "." + x.getFullYear(); 
    document.getElementById('date').innerHTML = date;

    var time = addZero(x.getHours()) + ":" + addZero(x.getMinutes()) + " Uhr";
    document.getElementById('time').innerHTML = time; 
    
    tt=display_c();
}