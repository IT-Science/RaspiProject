##########################################################################################################################################
#   Globals:    - events = array with all events of the current week
#               - string = html-code, which is later written into the following html-files:
#
#               - calendar = string with the written custos.ics-Datei
#   Functions:
#               - update_calendar():
#                       parameters:
#                           none
#                       used imported Modules:
#                           urllib.request
#                       used global variables:
#                           calendar
#                       most important local variables:
#                           cal = link to the current custos.ics-Datei
#                       short description:
#                           reading the ical-file saving it in the variable calendar for local use
#               - get_information():
#                       parameters:
#                           days: nuber of days of events that is read from calender
#                       used imported Modules:
#                           re
#                           time
#                           datetime
#                       used global variables: 
#                           events
#                           calendar
#                       most important local variables:
#                           date = string with current date in the fomrat YYYYMMDD 
#                           event = dictionary in the form {"Year":"", "Month": "", "Day": "", "FullDay": "", 
#                                   "HourStart":"", "MinuteStart":"", "HourEnd":"", "MinuteEnd":"", 
#                                   "Location":"", "Description":""} 
#                       short description:
#                           with the help of a regex it reads out the most important informations of the event of the week
#                           and saves them in the global variable "events" as an array 
#                - sort_events():
#                       parameters:
#                           events (!not the global one)
#                       used imported modules:
#                            none
#                       used global variables:
#                            none
#                       most important local variables::
#                            none
#                       short description:
#                            it is called by create_string() and takes the formerly unsorted array events and sorts the array after the 
#                            starting hour of the events, so that they later are displayed in a nice order
#                - create_string():
#                       parameters:
#                           days: number of days that should be written into the html-file
#                           cssFile: the css-File that should be used in the html
#                           exception: if an arror occurs on the Try-Catch construction below the exception
#                                     is written and displayed in the string
#                       used importet Modules:
#                           none
#                       used global variables:
#                           string
#                           events
#                       used other functions:
#                           sort_events()
#                       most important local variables:
#                           none
#                       short description:
#                            depending on the number of days of events that should be displayed
#                            the function creates a string with the html-file and stores it in the global
#                            variable "string". It first of all creates two different html-headers depending on 
#                            wether only one day of events is reqeusted or several days of events. Than it reads 
#                            out the events depending on the number of days while at the same time sorting them after their beginning time
#               - write_string():
#                       parameters:
#                           writeTo: the html-file where the string should be written into
#                       used importet Modules:
#                            os
#                            os.path dirname and abspath
#                       used global variables:
#                            string
#                       most importatn local variables:
#                            none
#                       short discription:
#                            writing the in create_string() created string into the writeTo file
#              - start_the_program():
#                      parametes:
#                           writeTo1/2: The parameter, where the html-file should be writte into, that is then given as a parameter to write_string()
#                           days1/2: number of days of events that should be displayed
#                           timeSleep=1200: time inveral for updates
#                           timeException=600: time interval for updates if an exception occured
#                            
#########################################################################################################################################

import urllib.request
import time
import re
import datetime
import os
from re import *
from os.path import dirname, abspath

events = []
string = ""
calendar = ""

def update_calendar():
    global calendar
    cal = "https://custos.drs.de/S5jKxAtXt3.ics"
    with urllib.request.urlopen(cal) as response:
        calendar = response.read().decode("utf-8")

def get_information(days):
    global events
    global calendar

    #k stands for the number of days, that is added to the current date, so that the information of that date can be retrieved
    for k in range(0, days):
        date = int(time.strftime("%Y%m%d", time.localtime(time.time() + 24*k*3600)))
        matches = re.findall(r"(?s)DTSTART[;:]TZID=Europe/Berlin:{}.*?END:VEVENT".format(date), calendar)
        for i in matches:
            try:
                event = {"Year":"", "Month": "", "Day": "", "FullDay": "", "HourStart":"", "MinuteStart":"", "HourEnd":"", "MinuteEnd":"", "Location":"", "Description":""}

                #Getting the date and time of event begin
                days = ["Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag"]
                dtstart = re.search(r"(?s)(?<=DTSTART[:;]TZID=Europe/Berlin:){}T\d*".format(date), i).group(0)
                event["Year"] = dtstart[0:4]
                event["Month"] = dtstart[4:6]
                event["Day"] = dtstart[6:8]
                event["HourStart"] = dtstart[9:11]
                event["MinuteStart"] = dtstart[11:13]
                event["FullDay"] = days[datetime.datetime(int(dtstart[0:4]), int(dtstart[4:6]), int(dtstart[6:8])).weekday()]

                #Getting the date and time of event end
                dtend = re.search(r"(?s)(?<=DTEND[:;]TZID=Europe/Berlin:){}T\d*".format(date), i).group(0)
                event["HourEnd"] = dtend[9:11]
                event["MinuteEnd"] = dtend[11:13]

                #Getting the Summary of the event
                summary = re.search(r"(?<=SUMMARY:).*(?=/)", i).group(0)
                event["Description"] = summary


                #Getting the Location of the event
                location = re.search(r"(?<=LOCATION:).*(?=\()", i).group(0)
                event["Location"] = location

                #Adding the created event dictionary to the events list
                events.append(event)
            except Exception as e:
                continue
        print(events)

#sorting the events in the order of the starting hour
def sort_events(events):
    return sorted(events, key=lambda event: event["HourStart"])

#Creating the string for the html file
def create_string(days, cssFile, exception=None):
    global string
    global events

    string = ""
    #Depending on the number of days this creates the html-head and small parts of the body
    if days == 1:
        string = """<!DOCTYPE html>
    <html>
    <head>
    <meta charset="utf-8" />
    <meta lang="de" />
    <meta content="St. Georg" />
    <meta HTTP-EQUIV="refresh" CONTENT="1200"> 
    <link rel="stylesheet" type="text/css" href="../StyleSheets/""" + cssFile + """\"/>
    <script src="../Backends/DisplayDateAndTime.js"></script>
    <title>St. Georg Software</title>
    </head>
    <body onload=display_timedate()>
    <div class="top">
        <div id="headers">
        <h1>Willkommen in St. Georg</h1>
        <h2>Wir begrüßen heute:</h2>
        </div>
        <div>
        <p id= "date"></p>
        <p id="time"></p>
        </div>
    </div>
    <table>
        <thead>
        <tr>
            <th scope="col">Beschreibung</th>
            <th scope="col">Raum</th>
            <th scope="col">Uhrzeit</th>
        </tr>
        </thead>
        <tbody class="spacing"></tbody>
        <tbody class="content">"""
    else:
        string = """<!DOCTYPE html>
        <html>
        <head>
        <meta charset="utf-8" />
        <meta lang="de" />
        <meta content="St. Georg" />
        <meta HTTP-EQUIV="refresh" CONTENT="1200"> 
        <link rel="stylesheet" type="text/css" href="../StyleSheets/""" + cssFile + """\"/>
        <script src="DisplayDateAndTime.js"></script>
        <title>St. Georg Software</title>
        </head>
        <body onload=display_timedate()>
        <div class="top">
            <div id="headers">
            <h3>Wochenübersicht:</h3>
            </div>
            <div>
            <p id= "date"></p>
            <p id="time"></p>
            </div>
        </div>
        <table>
            <thead>
            <tr>
                <th scope="col">Beschreibung</th>
                <th scope="col">Raum</th>
                <th scope="col">Uhrzeit</th>
            </tr>
            </thead>
            <tbody class="spacing"></tbody>
            <tbody class="content">"""

    if exception == None:
        #j iterates through the events in event. The variable difDays shows how many days
        #the function iterated through so far. It ends, as soon as difDays is equal to days. If
        #days is equal to one the Day-heading will be left away. At the end events is
        #emptied, so that in the next iteration events can be filled from beginning.
        #as I want the events to be displayed in a correct order the code might seem a bit complicated at some points
        print(events)
        if len(events) == 0:
            print("hier")
            append = """
            <tr>
                <td>Es scheint so, als wären keine Veranstaltungen geplant.</td>
                <td></td>
                <td></td>
            </tr>"""
            string = string + append
        else:
            difDays = 0
            events_one_day = []
            for j in range(0, len(events)):

                #Case: first event
                if (difDays == 0 and days!=1):
                    difDays = difDays + 1
                    Day = """
                <tr id="Day"> 
                    <td id="Day">""" + events[j]["FullDay"] + """</td>
                    <td id="Day"></td>
                    <td id="Day"></td> 
                </tr>"""
                    string = string + Day

                #Case: all events with a different Day than the event before (except the first event)
                #First the events of the previous day are written to the string and than the the header for the next
                #day is added and events_one_day is emptied and filled again
                elif (events[j]["FullDay"] != events[(j-1)]["FullDay"] and days != 1): 
                    events_one_day = sort_events(events_one_day)
                    for k in range(0, len(events_one_day)):
                        append = """
                <tr>
                    <td>""" + events_one_day[k]["Description"] + """</td>
                    <td>""" + events_one_day[k]["Location"] + """</td>
                    <td>""" + events_one_day[k]["HourStart"] + ":" + events_one_day[k]["MinuteStart"] + " - " + events_one_day[k]["HourEnd"] + ":" + events_one_day[k]["MinuteEnd"] + """</td>
                </tr>"""
                        string = string + append

                    difDays = difDays + 1
                    Day = """
                <tr id="Day"> 
                    <td id="Day">""" + events[j]["FullDay"] + """</td>
                    <td id="Day"></td>
                    <td id="Day"></td> 
                </tr>"""
                    string = string + Day
                    events_one_day = []


                #Case: last event so that for the day the events are also written to the string
                elif j == (len(events) - 1):
                    events_one_day.append(events[j])
                    events_one_day = sort_events(events_one_day)
                    for k in range(0, len(events_one_day)):
                        append = """
                <tr>
                    <td>""" + events_one_day[k]["Description"] + """</td>
                    <td>""" + events_one_day[k]["Location"] + """</td>
                    <td>""" + events_one_day[k]["HourStart"] + ":" + events_one_day[k]["MinuteStart"] + " - " + events_one_day[k]["HourEnd"] + ":" + events_one_day[k]["MinuteEnd"] + """</td>
                </tr>"""
                        string = string + append
                events_one_day.append(events[j])

                
    else:
        append = """
            <tr>
                <td>The following error occured:</td>
                <td>""" + repr(exception) + """</td>
                <td></td>
            </tr>""" 
        string = string + append
    #Making the end of the html-file, so closing the body and the html
    string = string + """
        </tbody>
    </table>
    </body>
    </html>"""
    events = []

#Writing the string
def write_string(writeTo):
    data = open(os.path.dirname(os.getcwd()) + "/FrontEnds/" + writeTo, "w", encoding="utf-8")
    data.write(string)


#I guess updating the calendar every hour should be enough
def start_the_program(writeTo1, writeTo2, days1, days2, css1, css2, timeSleep=1200, timeException=600):
    while True:
        try:
            update_calendar()
            get_information(days1)
#            sort_events()
            create_string(days1, css1)
            write_string(writeTo1)
            get_information(days2)
#            sort_events()
            create_string(days2, css2)
            write_string(writeTo2)
            print("done")
            time.sleep(timeSleep)
        except Exception as e:
            create_string(days1, css1, e)
            write_string(writeTo1)
            create_string(days2, css2, e)
            write_string(writeTo2)
            print("excpetion")
            print(e) 
            time.sleep(timeException)

start_the_program("TodayOnly.html", "WeekOverview.html", 1, 7, "styleTodayOnly.css", "styleWeekOverview.css", 1200, 600)